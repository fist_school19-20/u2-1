# u2-1 (ПТ, 16:00-19:00, 423/3)
геймдизайн и программирование многопользовательских игр

## Теория:
[metanit](https://metanit.com/sharp/tutorial/1.1.php)
## Дистрибутивы:
* [Unity 2019 (Hub + Editor)](https://unity3d.com/get-unity/download/archive?_ga=2.165557631.1756698075.1571419811-102526927.1567534502)
* [Visial Studio Community 2019](https://visualstudio.microsoft.com/ru/thank-you-downloading-visual-studio/?sku=Community&rel=16)
* [Java 7+ (JDK)](https://drive.google.com/open?id=1NLye51A_7SrJ-lF3qm4dBGmREmt38mnz)
* [Android SDK](https://drive.google.com/open?id=11BZAOEtsnzeVTey64xUxaom7_VMEVEeH)
* Android Unity plugin





