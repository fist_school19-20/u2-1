﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    [SerializeField] int AmmoCount;
    void OnTriggerEnter(Collider other)
    {

        if (other.GetComponent<PlayerController>())
        {
            other.GetComponent<RangeWeapon>()
                .ChangeAmmo(0, AmmoCount);
            Destroy(gameObject);
        }
    }
}
