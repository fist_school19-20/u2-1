﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Interactable {

    public int damage;
    void OnCollisionEnter(Collision other)
    {
        if (other.collider.GetComponent<HP>())
            other.collider.GetComponent<HP>().GetDamage(damage);
        Use();

    }
    public override void Use()
    {
        base.Use();
        print("boom like " + damage + " points");
        Destroy(gameObject);
    }
}
