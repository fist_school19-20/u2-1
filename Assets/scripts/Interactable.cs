﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public string Name;
    public virtual void Use()
    {
        print(Name + " has been used");
    }
}
