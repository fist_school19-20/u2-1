﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RangeWeapon : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] Transform shootPoint;

    [SerializeField] int totalAmmo;
    [SerializeField] int currentClip;
    [SerializeField] int clipCapacity;
    [SerializeField] Text ammoText;
    void Start()
    {
        shootPoint = GetComponentInChildren<Camera>().transform;

        Reload();
    }

    public void Shoot()
    {
        if (currentClip <= 0)
            return;

        GameObject b = GameObject.Instantiate(
                bullet,
                shootPoint.position + shootPoint.forward,
                shootPoint.rotation
                );
        b.GetComponent<Rigidbody>().AddRelativeForce(
            Vector3.forward * 10f,
            ForceMode.Impulse
            );
        ChangeAmmo(-1, 0);
    }
    public void Reload()
    {
        int demand = clipCapacity - currentClip;

        if (totalAmmo >= demand)
        {
            ChangeAmmo(demand, -demand);
        }
        else
        {
            ChangeAmmo(totalAmmo, -totalAmmo);
        }
    }

    public void ChangeAmmo(int c, int t)
    {
        currentClip += c;
        totalAmmo += t;
        if (ammoText)
            ammoText.text = currentClip + "|" + totalAmmo.ToString("000");
    }
}
