﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    List<MapObject> mapObjects = new List<MapObject>();
    public GameObject player;
    public RectTransform radarPanel;
    public GameObject iconPrefab;
    [SerializeField] float scale;
    [SerializeField] float maxMagn;
    [SerializeField] float viewDestance;
    #region Singleton
    private static MapController _mapController;
    public static MapController Instance()
    {
        return _mapController;
    }
    void Awake()
    {
        if (MapController.Instance() == null)
            _mapController = this;
    }
    #endregion Singleton
    public void RegisterObject(MapObject mo)
    {
        GameObject m = GameObject.Instantiate(
            iconPrefab,
            radarPanel
        );
        m.GetComponent<Image>().sprite = mo.sprite;
        mo.icon = m.GetComponent<Image>();
        mapObjects.Add(mo);
    }
    public void RemoveObject(MapObject mo)
    {
        Destroy(mo.icon);
        mapObjects.Remove(mo);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 tmp = Vector3.zero;
        tmp.z = player.transform.rotation.eulerAngles.y;
        radarPanel.localRotation = Quaternion.Euler(tmp);

        foreach (MapObject mo in mapObjects)
        {
            Vector3 rel = Vector3.zero;
            rel.x = mo.owner.transform.position.x - player.transform.position.x;
            rel.y = mo.owner.transform.position.z - player.transform.position.z;

            if (rel.magnitude > viewDestance)
            {
                mo.icon.enabled = false;
                continue;
            }
            else
            {
                mo.icon.enabled = true;
            }

            rel *= scale;
            rel = Vector3.ClampMagnitude(rel, maxMagn);
            mo.icon.transform.localPosition = rel;

            mo.icon.transform.localRotation = Quaternion.Euler(-tmp);
        }

    }
}
