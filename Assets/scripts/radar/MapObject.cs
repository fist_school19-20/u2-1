﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapObject : MonoBehaviour
{
    public Sprite sprite;
    public GameObject owner;
    public Image icon;

    void Start()
    {
        // create icon on radar
        owner = gameObject;
        MapController.Instance().RegisterObject(this);
    }
    void OnDestroy()
    {
        // destroy icon
        MapController.Instance().RemoveObject(this);
    }
}
