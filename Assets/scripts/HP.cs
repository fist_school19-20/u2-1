﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour {
    [SerializeField] [Range(0, 100)] int maxHealth;
    [SerializeField] Image hpBar;
    int health;
	void Start () {
        health = maxHealth;
	}
    public void GetDamage(int dmg) {
        health -= dmg;
        if (hpBar)
            hpBar.fillAmount = (1f * health) / maxHealth;

        if (health <= 0) {
            Destroy(gameObject);
        }
        print(health);
    }
}
