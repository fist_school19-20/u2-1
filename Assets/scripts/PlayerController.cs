﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Text hint;
    RaycastHit hit;
    Camera cam;
    [SerializeField][Range(0.1f, 5f)] float InteractDistance;
    Interactable itemToInteract;
    void Start()
    {
        cam = GetComponentInChildren<Camera>();
    }
    void Update()
    {
        Physics.Raycast(
            cam.transform.position,
            cam.transform.forward,
            out hit,
            InteractDistance
            );
        if (hit.collider &&
            (itemToInteract = hit.collider.GetComponent<Interactable>()))
        {
            hint.text = "Press E to use " + itemToInteract.Name;
        }
        else
        {
            hint.text = "";
            itemToInteract = null;
        }


        if (Input.GetMouseButtonDown(0))
        {
            GetComponent<RangeWeapon>().Shoot();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            GetComponent<RangeWeapon>().Reload();
        }
        if (Input.GetKeyDown(KeyCode.E) && itemToInteract)
        {
            itemToInteract.Use();
        }
    }
}
