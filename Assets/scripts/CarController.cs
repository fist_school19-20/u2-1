﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : Interactable
{
    float h, v;
    bool driven = false;
    public override void Use()
    {
        base.Use();

        driven = !driven;
        print(driven);
    }
    [SerializeField] Transform centerOfMass;

    [SerializeField] List<WheelCollider> DriveWheels = 
        new List<WheelCollider>();
    [SerializeField]
    List<WheelCollider> SteerWheels =
        new List<WheelCollider>();
    // Start is called before the first frame update
    void Start()
    {
        //GetComponent<Rigidbody>().centerOfMass = centerOfMass.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (driven)
        {
            h = Input.GetAxis("Horizontal");
            v = Input.GetAxis("Vertical");
        } else
        {
            h = 0;
            v = 0;
        }

        foreach (WheelCollider wheel in DriveWheels)
        {
            wheel.motorTorque = v * 1000f;

            Quaternion q;
            Vector3 p;
            wheel.GetWorldPose(out p, out q);
            wheel.transform.GetChild(0).position = p;
            wheel.transform.GetChild(0).rotation = q;
        }
        foreach (WheelCollider wheel in SteerWheels)
        {
            wheel.steerAngle = h * 30f;
            Quaternion q;
            Vector3 p;
            wheel.GetWorldPose(out p, out q);
            wheel.transform.GetChild(0).position = p;
            wheel.transform.GetChild(0).rotation = q;

            /*
            wheel.transform.GetChild(1).rotation =  Quaternion.Euler(
                    0,
                    wheel.transform.GetChild(0).rotation.y,
                    0
                );
                */
        }
    }
}
